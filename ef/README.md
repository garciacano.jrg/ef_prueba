Esta prueba ha sido realizada principalmente con React y SCSS (SASS). Se han utilizado algunas clases de bootstrap pero no se ha seguido su patrón de maquetación.


Para arrancar el proyecto una vez clonado, se deben instalar las dependencias ejecutando el siguiente comando desde la carpeta raíz del proyecto:

npm i

Una vez que haya terminado, se podrá arrancar ejecutando el comando:

npm start