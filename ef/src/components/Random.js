import React from "react";
import data from "../data/data.json";

const Random = () => Object.values(data.random).map((item) => {
    return (
      <article key={item.post_id}>
        <div className="category-container-random">
          <h2 className="category-random">{item.category.name}</h2>
          <h2 className="subcategory-random">{item.subcategory.name}</h2>
        </div>
        <div className="title-summary-container desktop">
          <h1 className="title-random">
          {item.title}
          </h1>
          <h2 className="summary-random">
          {item.summary}
          </h2>
        </div>
        <div>
          <img src={item.cover_image.url} alt={item.post_id} />
        </div>
        <div className="title-summary-container mobile">
          <h1 className="title-random">
            {item.title}
          </h1>
          <h2 className="summary-random">
          {item.summary}
          </h2>
        </div>
        <div className="d-flex justify-content-between align-items-center">
          <div>
            <p>{item.author.name}</p>
            <p>
              <span>Date</span> {item.readtime}
            </p>
          </div>
          <div className="d-flex justify-content-end">
            <div className="facebook-logo-box">
              <img src="http://espanafascinante.com/test/icons/FB.png" alt="logo-facebook-white" />
            </div>
            <div className="share-logo-box">
              <img src="http://espanafascinante.com/test/icons/share.png" alt="logo-share-white" />
            </div>
          </div>
        </div>
      </article>
    );
  });

export default Random;
