import React from 'react';
import logoWhite from '../assets/LOGO_white.png';

const Footer = () => {
    return (
        <footer>
        <div className="container">
            <div className="logo-footer-container">
                <img src={logoWhite} alt="logo-footer" />
            </div>
            <div className="list-footer-container">
                <div>
                    <p>Category</p>
                    <ul>
                        <li>Subcategory 1</li>
                        <li>Subcategory 2</li>
                    </ul>
                </div>
                <div>
                    <p>Category</p>
                    <ul>
                        <li>Subcategory 1</li>
                        <li>Subcategory 2</li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    )
}

export default Footer;