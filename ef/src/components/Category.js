import React from "react";
import data from "../data/data.json";

const Category = () => Object.values(data.category).map((item) => {
    return (
      <article className="category-article" key={item.post_id}>
        <div className="category-container-category">
          <h2 className="category-category">{item.category.name}</h2>
          <h2 className="subcategory-category">{item.subcategory.name}</h2>
        </div>
        <h1 className="title-category">{item.title}</h1>
        <p className="summary-category desktop">{item.summary}</p>
        <img src={item.cover_image.url} alt={item.post_id} />
        <p className="summary-category">{item.summary}</p>
        <div className="sources-category">
          <div>
            <p className="text-nowrap">{item.author.name}</p>
            <p className="text-nowrap">
              <span>Date </span>
              {item.readtime}
            </p>
          </div>
          <div className="d-flex justify-content-end">
            <div className="images-container__facebook">
              <img src="http://espanafascinante.com/test/icons/FB.png" alt="logo-facebook" />
            </div>
            <div className="images-container__share">
              <img src="http://espanafascinante.com/test/icons/share.png" alt="logo-facebook" />
            </div>
          </div>
        </div>
      </article>
    );
  });

export default Category;
