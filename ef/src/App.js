import "./App.css";
import './scss/app.scss';
import { Fragment } from "react";
import Header from "./components/Header";
import Cover from "./components/Cover";
import Category from "./components/Category";
import Random from "./components/Random";
import Footer from "./components/Footer";

function App() {
  return (
    <Fragment>
      <Header></Header>
      <Cover></Cover>
      <section className="container">
        <h1 className="category-h1 mb-5">Category</h1>
        <div className="category-block">
          <Category></Category>
        </div>
        <h2 className="read-more">Read more</h2>
      </section>
      <section className="container">
        <div className="random-block">
          <Random></Random>
        </div>
      </section>
      <Footer></Footer>
    </Fragment>
  );
}

export default App;